<?php require_once './code.php' ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S4: Activity</title>
</head>
<body>

	<h1>Building</h1>
		<p><?= $building->printName(); ?></p>
		<p><?= $building->printFloors(); ?></p>
		<p><?= $building->printAddress(); ?></p>


		<p><?= $building->setName("Caswynn Complex"); ?></p>
		<p><?= $building->getName(); ?></p>



	<h1>Condominium</h1>
		<p><?= $condominium->printName(); ?></p>
		<p><?= $condominium->printFloors(); ?></p>
		<p><?= $condominium->printAddress(); ?></p>


		<p><?= $condominium->setName("Enzo Tower"); ?></p>
		<p><?= $condominium->getName(); ?></p>

</body>
</html>