<?php

class Building {
	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	// Class methods
	public function printName(){
		return "The name of the building is $this->name.";
	}

	public function printFloors(){
		return "The $this->name has $this->floors floors.";
	}

	public function printAddress(){
		return "The $this->name is located at $this->address.";
	}


	public function getName(){
		return "The name of the building has changed to $this->name.";
	}

	public function setName($name){
		$this->name = $name;
	}
}


class Condominium extends Building{
	public function printName(){
		return "The name of the condominium is $this->name.";
	}

	public function printFloors(){
		return "The $this->name has $this->floors floors.";
	}

	public function printAddress(){
		return "The $this->name is located at $this->address.";
	}


	public function getName(){
		return "The name of the condominium has changed to $this->name.";
	}

	public function setName($name){
		$this->name = $name;
	}
}


$building = new Building('Caswynn Building', 8, 'Quezon City, Philippines');
$condominium = new Condominium('Enzo Condo', 5, 'Makati City, Philippines');